import { callApi } from '../helpers/apiHelper';

class FighterService {
  fightersList = [];
  fightersData = new Map();
  async getFighters() {
    if (this.fightersList.length > 0){
      return this.fightersList;
    }
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      this.fightersList = JSON.parse(atob(apiResult.content));
      return this.fightersList;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    if(this.fightersData.has(_id)){
      return this.fightersData.get(_id);
    }
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
      this.fightersData.set(_id, JSON.parse(atob(apiResult.content)));
      return this.fightersData.get(_id)
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
