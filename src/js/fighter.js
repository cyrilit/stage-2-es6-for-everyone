import {random} from "./helpers"

export class Fighter {

    constructor(attack,defence,health,id,name){
        this.attack = attack;
        this.defense = defence;
        this.id = id;
        this.name = name;
        this.health = health;
    }

    getAttackPoints() {
        let criticalHitChance = random(1, 2);
        let damage = this.attack * criticalHitChance;
        return damage;
    }

    getDefencePoints() {
        let dodgeChance = random(1, 2);
        let power = this.defense * dodgeChance;
        return power;
    }

}