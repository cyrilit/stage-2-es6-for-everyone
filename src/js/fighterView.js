import View from './view';
import {fighterService} from './services/fightersService';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source, id } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const dataElement = this.createData(fighter);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(dataElement);
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createData(fighter){
    let attributes = {};
    const element = this.createElement({
      'tagName':'div',
      'className':'fighter-data',
      attributes
    });
    const health = this.createElement({
      'tagName':'div',
      'className':'fighter-health',
    });
    element.append(health);
    return element;
  }

}

export default FighterView;