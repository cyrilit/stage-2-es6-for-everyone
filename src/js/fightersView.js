import View from './view';
import FighterView from './fighterView';
import {fighterService} from './services/fightersService';

class FightersView extends View {
    constructor(fighters) {
        super();
        this.fighters = [];
        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }


    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            let data = fighterService.getFighterDetails(fighter._id);
                data.source = fighter.source;
                data.name = fighter.name;
            const fighterView = new FighterView(data, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {

        
        // get from map or load info and add to fightersMap
        // show modal with fighter info
        // allow to edit health and power in this modal
    }
}

export default FightersView;