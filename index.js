import App from './src/js/app';
import 'bootstrap-scss';
import './src/scss/app.scss';

let app = new App();
app.start();